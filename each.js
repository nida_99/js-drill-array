function each(elements, callback) {
    for (let index = 0; index < elements.length; index++) {
        callback(elements[index], index);
    }
};
function printElement(element, index) {
    console.log(element);
};

module.exports.each = each;
module.exports.printElement = printElement;