function map(elements, callback) {
    let newArray = [];
    for (let index = 0; index < elements.length; index++) {
        let val = callback(elements[index], index);
        newArray.push(val);
    }
    return newArray;
};

function multiplyEachElement(element, index) {
    return element * 2;
};

module.exports.map = map;
module.exports.multiplyEachElement = multiplyEachElement;