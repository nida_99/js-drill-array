function reduce(elements, callback, startingValue) {
    let accumulator;
    let position = 0;
    if (startingValue) {
        accumulator = startingValue;
    }
    else {
        accumulator = elements[0];
        position = 1;
    }
    for (let index = position; index < elements.length; index++) {
        accumulator = callback(accumulator, elements[index]);
    }
    return accumulator;
}
function sumOfArray(startingValue, element) {
    return element + startingValue;
}
module.exports.reduce = reduce;
module.exports.sumOfArray = sumOfArray;