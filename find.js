function find(elements, callback) {

  for (let index = 0; index < elements.length; index++) {
    if (callback(elements[index], index)) {
      return elements[index];
    }
  }
  return undefined;
};

function greaterThanZero(element, index) {

  if (element > 0) {
    return true;
  }
};

module.exports.find = find;
module.exports.greaterThanZero = greaterThanZero;







