function filter(elements, callback) {
    let newArray = [];
    for (let index = 0; index < elements.length; index++) {
        let value = callback(elements[index], index);
        if (value) {
            newArray.push(elements[index]);
        }
    }
    return newArray;
};

function checkIfEven(element, index) {
    if (element % 2 === 0) {
        return true;
    }

};

module.exports.filter = filter;
module.exports.checkIfEven = checkIfEven;