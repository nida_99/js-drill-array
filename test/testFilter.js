const filter = require("../filter.js").filter;
const checkIfEven = require("../filter.js").checkIfEven;

let correctAnswer = true;

const items = [1, 2, 3, 4, 5, 5];
const expectedOutput = [2, 4];

const actualOutput = filter(items, checkIfEven);

for (let index = 0; index < actualOutput.length; index++) {
    if (actualOutput[index] != expectedOutput[index]) {
        correctAnswer = false;
    }
}

if (correctAnswer) {
    console.log("Filter function is working properly");
}
else {
    console.error("Check the function again!");
}