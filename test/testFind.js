const find = require("../find.js").find;
const greaterThanZero = require("../find.js").greaterThanZero;


const items = [1, 2, 3, 4, 5, 5];

const expectedOutput = 1;
const actualOutput = find(items, greaterThanZero);

if (expectedOutput === actualOutput) {
    console.log("Find function is working properly");
}
else {
    console.log("Check the fuction again!");
}
