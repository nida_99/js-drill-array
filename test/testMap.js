const map = require("../map.js").map;
const multiplyEachElement = require("../map.js").multiplyEachElement;

const items = [1, 2, 3, 4, 5, 5];
let correctAnswer = true;

const expectedOutput = [2, 4, 6, 8, 10, 10];
let actualOutput = map(items, multiplyEachElement);

for (let index = 0; index < actualOutput.length; index++) {
    if (actualOutput[index] !== expectedOutput[index]) {
        correctAnswer = false;
    }
}

if (correctAnswer) {
    console.log("Map function is working properly");
}
else {
    console.error("Check the function again!");
}



