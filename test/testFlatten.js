const flatten = require('../flatten.js');
const nestedArray = [1, [2], [[3]], [[[4]]]];

let correctAnswer = true;
const expectedOutput = [1, 2, 3, 4];
const actualOutput = flatten(nestedArray);

for (let index = 0; index < actualOutput.length; index++) {
    if (actualOutput[index] !== expectedOutput[index]) {
        correctAnswer = false;
    }
}

if (correctAnswer) {
    console.log("Flatten function is working properly");
}
else {
    console.log("Check again!");
}