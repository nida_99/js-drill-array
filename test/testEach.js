const each = require("../each.js").each;
const printElement = require("../each.js").printElement;
const items = [1, 2, 3, 4, 5, 5];

const expectedOutput = undefined;
const actualOutput = each(items, printElement);

if (actualOutput === undefined) {
    console.log("each Function is properly working");
}
else {
    console.error("Check function again.");
}
