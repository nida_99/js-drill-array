const reduce = require("../reduce.js").reduce ;
const sumOfArray = require("../reduce.js").sumOfArray;

const items = [1, 2, 3, 4, 5, 5];
const expectedOutput = 20;

const actualOutput = reduce(items, sumOfArray, 0);

if (actualOutput === expectedOutput) {
    console.log("Reduce function is working properly");
}
else {
    console.log("Check your function again.")
}



